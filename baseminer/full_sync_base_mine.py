#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
BaseMiner.

syncs Basecamp with Redmine and vice versa
"""

import redmine
import requests
import urlobject
from requests.auth import HTTPBasicAuth


def main():
    """
    Main Fuction.

    Does all the work
    :rtype: none
    """
    # basecamp config
    basecamp = {
        "scheme": "https",
        "hostname": "basecamp.com",
        "path": "api/v1",
        "id": "2584039",
        "endpoints": {
            'projects': "projects.json",
            'people': "people.json"
        },
        "headers": {
            "User-Agent": "BaseMiner Sync (martin.toennishoff@scheideanstalt.de)",
        },
        "auth": {
            "user": "martin.toennishoff@scheideanstalt.de",
            "password": "mefoco43x"
        }
    }

    # redmine config
    redmine_cfg = {
        "url": "http://localhost:3000/redmine",
        "version": "3.2.0",
        "key": "",
    }

    mine = redmine.Redmine(
        redmine_cfg["url"],
        version=redmine_cfg["version"],
        key=redmine_cfg["key"]
    )

    # build URL from config
    url = urlobject.URLObject(
    ).with_scheme(
        basecamp["scheme"]
    ).with_hostname(
        basecamp["hostname"]
    ).add_path(
        basecamp["id"]
    ).add_path(
        basecamp["path"]
    ).add_path(
        basecamp["endpoints"]["people"]
    )

    # get project list
    request = requests.get(
        url,
        headers=basecamp["headers"],
        auth=HTTPBasicAuth(
            basecamp["auth"]["user"],
            basecamp["auth"]["password"]
        )
    )

    # stuff it into redmine
    for user in request.json():
        # @Todo: admin users -> group/role?
        # @Todo: trashed users -> group/role?
        name = user["name"].split(' ')

        r_user = mine.user.new()
        r_user.login = user["email_address"].split('@')[0]
        r_user.password = "47110815"
        r_user.firstname = ' '.join(name[::len(name) - 1])
        r_user.lastname = name[-1]
        r_user.mail = user["email_address"]
        r_user.auth_source_id = user["identity_id"]
        r_user.mail_notification = "only_my_events"
        r_user.must_change_passwd = True
        r_user.custom_fields = user
        # r_user.save()

        print repr(r_user)

        # print request.headers
        # print request.status_code
        # print request.json()


if __name__ == '__main__':
    main()
